﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PluggedInPortal.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PluggedInDataAccess;
using System.Web.Mvc;

namespace PluggedInPortal.Controllers.Tests
{
    [TestClass()]
    public class EventControllerTests
    {
        [TestMethod()]
        public void Index_Returns_EventList()
        {

            //Arrange
            EventController ec = new EventController();
            Type EXPECTED_LIST = typeof(List<Event>);
            //Act
            ViewResult result = ec.Index() as ViewResult;
            Type ACTUAL_LIST = result.Model.GetType();

            //Assert
            Assert.AreEqual(EXPECTED_LIST, ACTUAL_LIST);
        }
    }
}