﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PluggedInPortal.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using PluggedInDataAccess;
using PluggedInPortal.Models.ViewModels;
using System.Net;

namespace PluggedInPortal.Controllers.Tests
{
    [TestClass()]   
    public class HomeControllerTests
    {
        [TestMethod()]
        public void Index_Returns_IndexView()
        {
            //Arrange
            HomeController hc = new HomeController();
            string EXPECTED_VIEW = "index";
            //Act
            ViewResult result = hc.Index() as ViewResult;
            String ACTUAL_VIEW = result.ViewName;
            //Assert
            Assert.AreEqual(EXPECTED_VIEW, ACTUAL_VIEW);
        }

        [TestMethod()]
        public void About_Returns_AboutView()
        {
            //Arrange
            HomeController hc = new HomeController();
            string EXPECTED_VIEW = "about";
            //Act
            ViewResult result = hc.About() as ViewResult;
            String ACTUAL_VIEW = result.ViewName;
            //Assert
            Assert.AreEqual(EXPECTED_VIEW, ACTUAL_VIEW);
        }


        [TestMethod()]
        public void Contact_Returns_ContactView()
        {
            //Arrange
            HomeController hc = new HomeController();
            string EXPECTED_VIEW = "contact";
            //Act
            ViewResult result = hc.Contact() as ViewResult;
            String ACTUAL_VIEW = result.ViewName;
            //Assert
            Assert.AreEqual(EXPECTED_VIEW, ACTUAL_VIEW);
        }


        [TestMethod()]
        public void CreateAgencyApplication_Returns_AgencyResourceViewModel()
        {
            //Arrange
            HomeController hc = new HomeController();
            Type EXPECTED_MODEL = typeof(AgencyResourceViewModel);
            //Act
            ViewResult result = hc.CreateAgencyApplication("b7dbeffc-3b07-4e1b-9097-5b2f6f48113f") as ViewResult;
            Type ACTUAL_MODEL = result.Model.GetType();
            //Assert
            Assert.AreEqual(EXPECTED_MODEL, ACTUAL_MODEL);
        }

        [TestMethod()]
        public void CreateAgencyApplication_Returns_UserNotFoundError()
        {
            //Arrange
            HomeController hc = new HomeController();
            int EXPECTED_STATUS_CODE = new HttpStatusCodeResult(HttpStatusCode.NotFound).StatusCode;
            //Act
            HttpStatusCodeResult r = hc.CreateAgencyApplication("0asdasfa-asas12-r12rwqr-124-qwe12") as HttpStatusCodeResult;
            int ACTUAL_STATUS_CODE = r.StatusCode;
            //Assert
            Assert.AreEqual(EXPECTED_STATUS_CODE, ACTUAL_STATUS_CODE);
        }

        [TestMethod()]
        public void CreateAgencyApplication_Returns_BadRequest()
        {
            //Arrange
            HomeController hc = new HomeController();
            int EXPECTED_STATUS_CODE = new HttpStatusCodeResult(HttpStatusCode.NotFound).StatusCode;
            //Act
            HttpStatusCodeResult r = hc.CreateAgencyApplication("017969a4-24b8-45e4-aae2-544bdc709ddf") as HttpStatusCodeResult;
            int ACTUAL_STATUS_CODE = r.StatusCode;
            //Assert
            Assert.AreEqual(EXPECTED_STATUS_CODE, ACTUAL_STATUS_CODE);
        }

        [TestMethod()]
        public void CreateEvent_Returns_AgencyEventViewModel()
        {
            //Arrange
            HomeController hc = new HomeController();
            Type EXPECTED_MODEL = typeof(AgencyEventsViewModel);
            //Act
            ViewResult result = hc.CreateEvent(1) as ViewResult;
            Type ACTUAL_MODEL = result.Model.GetType();
            //Assert
            Assert.AreEqual(EXPECTED_MODEL, ACTUAL_MODEL);
        }


        [TestMethod()]
        public void CreateEvent_nullId_Returns_NotFoundError()
        {
            //Arrange
            HomeController hc = new HomeController();
            int EXPECTED_STATUS_CODE = new HttpStatusCodeResult(HttpStatusCode.NotFound).StatusCode;
            //Act
            HttpStatusCodeResult r = hc.CreateEvent(-1) as HttpStatusCodeResult;
            int ACTUAL_STATUS_CODE = r.StatusCode;
            //Assert
            Assert.AreEqual(EXPECTED_STATUS_CODE, ACTUAL_STATUS_CODE);
        }

        [TestMethod()]
        public void ManageEvent_nullId_Returns_BadRequestError()
        {
            //Arrange
            HomeController hc = new HomeController();
            int EXPECTED_STATUS_CODE = new HttpStatusCodeResult(HttpStatusCode.BadRequest).StatusCode;
            //Act
            HttpStatusCodeResult r = hc.ManageEvent(null) as HttpStatusCodeResult;
            int ACTUAL_STATUS_CODE = r.StatusCode;
            //Assert
            Assert.AreEqual(EXPECTED_STATUS_CODE, ACTUAL_STATUS_CODE);
        }

        [TestMethod()]
        public void DeleteEvent_nullId_Returns_BadRequestError()
        {
            //Arrange
            HomeController hc = new HomeController();
            int EXPECTED_STATUS_CODE = new HttpStatusCodeResult(HttpStatusCode.BadRequest).StatusCode;
            //Act
            HttpStatusCodeResult r = hc.DeleteEvent(null, null) as HttpStatusCodeResult;
            int ACTUAL_STATUS_CODE = r.StatusCode;
            //Assert
            Assert.AreEqual(EXPECTED_STATUS_CODE, ACTUAL_STATUS_CODE);
        }

        [TestMethod()]
        public void ManageEvent_Returns_AgencyModel()
        {
            //Arrange
            HomeController hc = new HomeController();
            Type EXPECTED_MODEL = typeof(Agency);
            
            //Act
            ViewResult result = hc.ManageEvent(1) as ViewResult;
            Type ACTUAL_MODEL = result.Model.GetType();
            
            //Assert
            Assert.AreEqual(EXPECTED_MODEL, ACTUAL_MODEL);
        }

        [TestMethod()]

        public void DeleteEvent_RedirectsToAction_ManageEvent()
        {
            //Arrange
            HomeController hc = new HomeController();
            string EXPECTED_ACTION = "ManageEvent";
            //Act
            RedirectToRouteResult result = (RedirectToRouteResult) hc.DeleteEvent(10, 2);
            //Assert
            Assert.AreEqual(EXPECTED_ACTION, result.RouteValues["action"]);
        }
    }
}