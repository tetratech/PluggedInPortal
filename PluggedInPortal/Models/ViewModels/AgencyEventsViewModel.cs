﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PluggedInDataAccess;

namespace PluggedInPortal.Models.ViewModels
{
    public class AgencyEventsViewModel
    {
        public Agency agency { get; set; }

        public Event agencyEvent { get;set; }
    }
}