﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PluggedInDataAccess;
namespace PluggedInPortal.Models.ViewModels
{
    public class AgencyResourceViewModel
    {
        public Agency agency { get; set; }
        public List<AgencyResourceMap> agencyResourceMaps { get; set; }

        public AgencyResourceViewModel(){
            agencyResourceMaps = new List<AgencyResourceMap>();    
        }
    }

    public class AgencyResourceMap
    {
        public int Id { get; set; }
        public string Name { set; get; }
        public bool isResourceProvided { get; set; }
    }
}