﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PluggedInPortal.Models
{
    public class Metrics
    {
        public int TotalUsersCount { get; set; }
        public int PendingApprovalCount { get; set; }
        public int ApplicationsRejectedCount { get; set; }
        public int TotalEventsCount { get; set; }
        public int UpcomingEventCount { get; set; }
        public int BannerCount { get; set; }
    }
}