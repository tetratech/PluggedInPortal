﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PluggedInDataAccess;
using PluggedInPortal.Controllers.CustomAuthorizaton;
using PluggedInPortal.Controllers;
using PluggedInPortal.Models;
using PluggedInPortal.Models.ViewModels;
using System.Net;
using System.Device;
using System.Device.Location;

namespace PluggedInPortal.Controllers
{

    [Authorize]
    public class HomeController : Controller
    {
        PluggedInEntities db = new PluggedInEntities();
        public ActionResult Index()
        {
            if(User.IsInRole("Agency"))
            {
                AspNetUser user = (AspNetUser)db.AspNetUsers.Where(a => a.Email == User.Identity.Name).First();
                Agency agency = db.Agencies.Find(user.Agency.Id);
                //ViewBag.AgencyId = agency.Id;
                return View("ViewProfile", agency);
            }

            if (User.IsInRole("Administrator"))
            { 
                return RedirectToAction("Index", "Admin");
            }
            else
            {
                return RedirectToAction("HomePage", "Home");
            }

            
        }

        [AllowAnonymous]
        public ActionResult HomePage()
        {
            List<Banner> banners = new List<Banner>();
            banners = db.Banners.ToList();
            IEnumerable<SelectListItem> items = db.Resources.Select(c => new SelectListItem
            {
                Value = c.Id.ToString(),
                Text = c.Name

            });

            ViewBag.ResourceList = items;

            return View(banners);
        }


        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View("about");
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View("contact");
        }

        [CustomAuthorize (Roles = "Agency")]
        public ActionResult CreateAgencyApplication(string id)
        {
            AspNetUser user = db.AspNetUsers.Find(id);
            ViewBag.Message = "Agency Appplication Page for User: " + id;
            if (user == null)
            {
                return HttpNotFound();
            }
            Agency agency = db.Agencies.Find(user.Agency.Id);
            if(agency == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AgencyResourceViewModel arvm = new AgencyResourceViewModel();
            arvm.agency = agency;
            foreach(Resource r in db.Resources.ToList())
            {
                AgencyResourceMap arm = new AgencyResourceMap();
                arm.Id = r.Id;
                arm.Name = r.Name;
                arm.isResourceProvided = agency.Resources.Contains(r);
                arvm.agencyResourceMaps.Add(arm);
            }
            
            return View(arvm);

        }

        [CustomAuthorize(Roles = "Agency")]
        [HttpPost]
        public ActionResult CreateAgencyApplication(AgencyResourceViewModel arvm, HttpPostedFileBase image, string StatusAction, string agencyLat, string agencyLong)
        {
            if(StatusAction == "PENDING REVIEW" || StatusAction == "SAVED")
            {
                if (ModelState.IsValid)
                {

                    Agency agency = db.Agencies.Find(arvm.agency.Id);
                    if (agency == null)
                        return new HttpStatusCodeResult(HttpStatusCode.NotFound);
                    agency.Name = arvm.agency.Name;
                    agency.PostalCode = arvm.agency.PostalCode;
                    agency.PhoneNumber = arvm.agency.PhoneNumber;
                    agency.WebsiteUrl = arvm.agency.WebsiteUrl;
                    agency.Description = arvm.agency.Description;
                    agency.Address = arvm.agency.Address;
                    agency.Latitude = Convert.ToDouble((agencyLat));
                    agency.Longtitude = Convert.ToDouble((agencyLong));

                    agency.ApplicationStatus = StatusAction;
                    if (image != null)
                    {
                        agency.ProfilePicture = new byte[image.ContentLength];
                        image.InputStream.Read(agency.ProfilePicture, 0, image.ContentLength);
                    }


                    db.Entry(agency).State = System.Data.Entity.EntityState.Modified;

                    //foreach (AgencyResourceMap arm in arvm.agencyResourceMaps)
                    for (int i = 0; i < arvm.agencyResourceMaps.Count; i++)
                    {
                        AgencyResourceMap arm = (AgencyResourceMap)arvm.agencyResourceMaps[i];
                        int id = arm.Id;
                        Resource r = db.Resources.Find(i + 1);
                        if (arm.isResourceProvided)
                        {
                            agency.Resources.Add(r);
                        }
                        else
                        {
                            agency.Resources.Remove(r);
                        }

                    }
                    db.SaveChanges();
                }
                else
                {
                    ViewBag.Message("Some Error Occured, please try again");
                }
                return RedirectToAction("CreateAgencyApplication", arvm);
            }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            
       }

        public ActionResult CreateEvent(int? agencyId)
        {
            if (agencyId == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            AgencyEventsViewModel aevm = new AgencyEventsViewModel();
            aevm.agency = db.Agencies.Find(agencyId);
            if (aevm.agency == null)
                return HttpNotFound();
            return View(aevm);
        }

        [HttpPost]
        public ActionResult CreateEvent(AgencyEventsViewModel aevm, string eventStartDate, string eventEndDate, string eventLat, string eventLong)
        {
            if (ModelState.IsValid)
            {
                //Agency agency = aevm.agency;
                Event e = aevm.agencyEvent;
                e.Agency = db.Agencies.Find(aevm.agency.Id);
                e.StartDate = Convert.ToDateTime(eventStartDate);
                e.EndDate = Convert.ToDateTime(eventEndDate);
                e.Latitude = Convert.ToDouble((eventLat));
                e.Longtitude = Convert.ToDouble((eventLong));

                db.Events.Add(e);
                db.SaveChanges();   
                //return View("CreateEvent", new { agencyId = aevm.agency.Id });
            }
            ViewBag.Name = "Add Event Success";
            ViewBag.AgencyId = aevm.agency.Id;
            return View();
           
        }


        public ActionResult ManageEvent(int? agencyId)
        {
            if (agencyId == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            Agency agency = db.Agencies.Find(agencyId);
            agency.Events = agency.Events.ToList();
            return View(agency);
        }


        public ActionResult DeleteEvent(int? eventId, int? agencyId)
        {
            if (agencyId == null || eventId == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            Event e = db.Events.Find(eventId);
            db.Events.Remove(e);
            db.SaveChanges();
            return RedirectToAction("ManageEvent", new { agencyId = agencyId });
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult SearchAgency(int? ResourceList, string userLat, string userLong)
        {
            IEnumerable<SelectListItem> items = db.Resources.Select(c => new SelectListItem
            {
                Value = c.Id.ToString(),
                Text = c.Name

            });

            List<Agency> agencies = new List<Agency>();
            agencies = db.Agencies.ToList();

            List<Agency> newList = new List<Agency>();

            Resource r = new Resource();
            r = db.Resources.Find(ResourceList);

            foreach (Agency a in agencies)
            {
                if (a.Resources.Contains(r) && (a.Longtitude != null && a.Latitude != null) && a.ApplicationStatus == "APPROVED")
                {
                    if(userLat != "" && userLong != "") { 
                        var sCoord = new GeoCoordinate(Convert.ToDouble(userLat), Convert.ToDouble(userLong));
                        var eCoord = new GeoCoordinate(Convert.ToDouble(a.Latitude), Convert.ToDouble(a.Longtitude));
                        a.Distance = Math.Round((sCoord.GetDistanceTo(eCoord) / 1000), 2);
                    }
                    newList.Add(a);
                }
            }

            List<Agency> sortedList = new List<Agency>();

            if (userLat != "" && userLong != "")
            {
                sortedList = newList.OrderBy(o => o.Distance).ToList();
            }
            else
            {
                sortedList = newList;
            }

            if (sortedList.Count() > 0)
            {
                ViewBag.NumberOfResults = "Results";
                ViewBag.ResourceList = items;

                return View("SearchAgency", sortedList);
            }
            else
            {
                ViewBag.NumberOfResults = "No Results Found";
                ViewBag.ResourceList = items;

                return View("SearchAgency");
            }


        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult ViewProfile(int? agencyId)
        {
            if (agencyId == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            Agency agency = db.Agencies.Find(agencyId);

            List<Resource> r = new List<Resource>();

            r = agency.Resources.ToList();
            ViewBag.ResourceList = r;

            return View("ViewProfile", agency);
        }

        [CustomAuthorize(Roles = "Agency")]
        public ActionResult ViewProfileAgency(int? agencyId)
        {
            if (agencyId == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            Agency agency = db.Agencies.Find(agencyId);

            List<Resource> r = new List<Resource>();

            r = agency.Resources.ToList();
            ViewBag.ResourceList = r;

            return View("ViewProfile", agency);
        }


        [HttpPost]
        [AllowAnonymous]
        public ActionResult GetDirections(int? agencyId)
        {
            Agency agency = db.Agencies.Find(agencyId);

            return View("GetDirections", agency);
        }

    }
}