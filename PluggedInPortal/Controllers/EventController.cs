﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PluggedInDataAccess;
using System.Device;
using System.Device.Location;

namespace PluggedInPortal.Controllers
{
    public class EventController : Controller
    {
        PluggedInDataAccess.PluggedInEntities db = new PluggedInDataAccess.PluggedInEntities();
        // GET: Event
        public ActionResult Index()
        {
            List<Event> events = new List<Event>();
            events = db.Events.ToList();
            IEnumerable<SelectListItem> items = db.Resources.Select(c => new SelectListItem
            {
                Value = c.Id.ToString(),
                Text = c.Name
            });

            ViewBag.NumberOfResults = "Results";
            ViewBag.ResourceList = items;

            return View(events);
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult SearchEvents(int? ResourceList, string userLat, string userLong)
        {
            IEnumerable<SelectListItem> items = db.Resources.Select(c => new SelectListItem
            {
                Value = c.Id.ToString(),
                Text = c.Name
            });

            List<Event> events = new List<Event>();
            events = db.Events.ToList();

            

            List<Event> newList = new List<Event>();

            Resource r = new Resource();
            r = db.Resources.Find(ResourceList);

            foreach (Event a in events)
            {
                if (a.ResourceId == r.Id && (a.Longtitude != null && a.Latitude != null))
                {
                    if(userLat != "" && userLong != "")
                    {
                        var sCoord = new GeoCoordinate(Convert.ToDouble(userLat), Convert.ToDouble(userLong));
                        var eCoord = new GeoCoordinate(Convert.ToDouble(a.Latitude), Convert.ToDouble(a.Longtitude));
                        a.Distance = Math.Round((sCoord.GetDistanceTo(eCoord) / 1000), 2);
                    }
                    newList.Add(a);
                }
            }

            List<Event> sortedList = new List<Event>();

            if (userLat != "" && userLong != "")
            {
                sortedList = newList.OrderBy(o => o.Distance).ToList();
            }
            else
            {
                sortedList = newList;
            }  

            if (sortedList.Count() > 0)
            {
                ViewBag.NumberOfResults = "Results";
                ViewBag.ResourceList = items;

                return View("Index", sortedList);
            }
            else
            {
                ViewBag.NumberOfResults = "No Results Found";
                ViewBag.ResourceList = items;

                return View("Index");
            }


        }
    }
}