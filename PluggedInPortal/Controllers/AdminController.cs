﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PluggedInDataAccess;
using System.Net;

namespace PluggedInPortal.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class AdminController : Controller
    {
        PluggedInEntities db = new PluggedInEntities();
        // GET: Admin
        public ActionResult Index()
        {
            Models.Metrics metrics = new Models.Metrics
            {
                TotalUsersCount = db.AspNetUsers.Count() - 1,
                ApplicationsRejectedCount = db.Agencies.Where(a => a.ApplicationStatus == "REJECTED").Count(),
                PendingApprovalCount = db.Agencies.Where(a => a.ApplicationStatus == "PENDING REVIEW").Count(),
                TotalEventsCount = db.Events.Count(),
                UpcomingEventCount = db.Events.Where(e => e.StartDate > DateTime.Now).Count(),
                BannerCount = db.Banners.Count()
            };
            return View(metrics);
        }


        public ActionResult ViewPendingApplications()
        {
            IEnumerable<Agency> allAgenciesPendingReview = new List<Agency>();
            allAgenciesPendingReview = db.Agencies.Where(a => a.ApplicationStatus == "PENDING REVIEW").ToList();
            return View(allAgenciesPendingReview);

        }

        public ActionResult ViewRejectedApplications()
        {
            IEnumerable<Agency> rejectedAgencyApplications = new List<Agency>();
            rejectedAgencyApplications = db.Agencies.Where(a => a.ApplicationStatus == "REJECTED").ToList();
            return View(rejectedAgencyApplications);
        }

        public ActionResult ViewAllEvents()
        {
            IEnumerable<Event> events = new List<Event>();
            events = db.Events.OrderByDescending(e => e.StartDate).ToList();
            return View(events);
        }

        public ActionResult ViewUsers()
        {
            IEnumerable<AspNetUser> users = new List<AspNetUser>();
            users = db.AspNetUsers.Where(u=> u.Email != "administrator@youthemployment.portal").ToList();
            if(TempData["LastAction"] != null)
            {
                ViewBag.LastAction = TempData["LastAction"].ToString();
            }
            return View(users);
        }

        public ActionResult DisableUser(string id)
        {
            if(id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AspNetUser user = db.AspNetUsers.Find(id);
            if(user == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);
            }

            //db.Entry(user).State = System.Data.Entity.EntityState.Modified;
            //user.LockoutEndDateUtc = DateTime.Now.AddDays(7);
            //db.SaveChanges();
            return View(user);
        }

        [HttpPost]
        public ActionResult DisableUser(AspNetUser user, DateTime EndDate)
        {
            AspNetUser u = db.AspNetUsers.Find(user.Id);
            if (u == null)
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);
            db.Entry(u).State = System.Data.Entity.EntityState.Modified;
            u.LockoutEndDateUtc = EndDate;
            db.SaveChanges();
            TempData["LastAction"] = u.Email + " has been locked";

            return RedirectToAction("ViewUsers");
        }

        public ActionResult UnlockUser(string id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            AspNetUser user = db.AspNetUsers.Find(id);
            if (user == null)
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);
            db.Entry(user).State = System.Data.Entity.EntityState.Modified;
            user.LockoutEndDateUtc = DateTime.Now;
            db.SaveChanges();
            TempData["LastAction"] = user.Email + " is now unlocked.";
            return RedirectToAction("ViewUsers");
        }

        public ActionResult ViewApplication(int? AgencyId)
        {
            if (AgencyId == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Agency agency = db.Agencies.Find(AgencyId);
            if (agency == null)
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);
            return View(agency);

        }

        public ActionResult ApproveApplication(int? AgencyId)
        {
            if(AgencyId == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Agency agency = db.Agencies.Find(AgencyId);
            if (agency == null)
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);


            db.Entry(agency).State = System.Data.Entity.EntityState.Modified;
            agency.ApplicationStatus = "APPROVED";
            db.SaveChanges();

            return RedirectToAction("ViewPendingApplications");


        }

        public ActionResult RejectApplication(int? AgencyId)
        {
            if (AgencyId == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Agency agency = db.Agencies.Find(AgencyId);
            if (agency == null)
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);


            db.Entry(agency).State = System.Data.Entity.EntityState.Modified;
            agency.ApplicationStatus = "REJECTED";
            db.SaveChanges();

            return RedirectToAction("ViewPendingApplications");


        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}