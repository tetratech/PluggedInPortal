# PluggedInPortal

PluggedInPortal is a portal that residents of Mississauga can use to search for 
non-profit agencies around their area that offers the services they need. Residents
can also search for upcoming events.

## Iteration 1

For iteration 1, we focused on the following tasks:
* Login
* Registration 


## Iteration 2

For iteration 2, we focused on the following tasks:
* Homepage (We are still waiting for the assets from our client)
* Agency Applications 
* Add Events
* Edit Events
* Display Events

## Iteration 3 

For iteration 3, we focused on the following tasks:
* Search agency
* Search events

## Iteration 4

For iteration 4, we have a working prototype of the system with the following additional features from previous iteration:
* Search Agency by Postal Code
* Search Events by Postal Code
* Add/Delete/Edit Banners
* Review Agency Application
* User Interface Enchancements
* Admin Dashboard

## Iteration 5

Final System Presentation
